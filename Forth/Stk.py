''' Stack.py handles the stack of the forth implementation '''

# TODO: check wrm Err undefined is ("import problem?")
import Forth.Err as Err


class Stack(object):
    ''' This class handles the stack of the forth implementation '''
    
    def __init__(self):
        self.stack = []

        
    def put(self, char):
        self.stack.insert(0, char)


    def prt(self):
        if len(self.stack) > 0:
            print(self.pop())
        else:
            Err.throwerror("stack empty", "stack")


    def pop(self):
        if len(self.stack) > 0:
            return self.stack.pop(0)
        else:
            Err.throwerror("stack empty", "stack")

            
    def show(self):
        text = ''
        if len(self.stack) > 0:
            for key in self.stack:
                text = text + ' ' + str(key)

        print(text + ' <' + str(len(self.stack)) + '>')


    def add(self):
        if len(self.stack) >= 2:
            arg1 = self.pop()
            arg2 = self.pop()
        
            try:
                self.put(int(arg1) + int(arg2))
            except:
                self.put(arg1)
                self.put(arg2)
                Err.throwerror("number required on stack", "stack")
        else:
            Err.throwerror("missing arguments on stack", "stack")


    def sup(self):
        if len(self.stack) >= 2:
            arg1 = self.pop()
            arg2 = self.pop()

            try:
                self.put(int(arg1) - int(arg2))
            except:
                Err.throwerror("number required on stack", "stack")
        else:
            Err.throwerror("missing arguments on stack", "stack")
                    

    def mul(self):
        if len(self.stack) >= 2:
            arg1 = self.pop()
            arg2 = self.pop()

            try:
                self.put(int(arg1) * int(arg2))
            except:
                Err.throwerror("number required on stack", "stack")
        else:
            Err.throwerror("missing arguments on stack", "stack")
            

    def div(self):
        if len(self.stack) >= 2:
            arg1 = self.pop()
            arg2 = self.pop()

            try:
                self.put(int(arg1) / int(arg2))
            except:
                Err.throwerror("number required on stack", "stack")
        else:
            Err.throwerror("missing arguments on stack", "stack")
            

    def drop(self):
        if len(self.stack) >= 1:
            self.pop()
        else:
            Err.throwerror("stack is empty", "stack")
        

    def swap(self):
        if len(self.stack) >= 2:
            arg1 = self.pop()
            arg2 = self.pop()

            self.put(arg1)
            self.put(arg2)
        else:
            Err.throwerror("missing arguments on stack", "stack")
            

    def negate(self):
        if len(self.stack) >= 1:
            arg1 = self.pop()
            arg1 = -arg1
            self.put(arg1)
        else:
            Err.throwerror("missing arguments on stack", "stack")


    def dup(self):
        if len(self.stack) >= 1:
            arg1 = self.pop()
            self.put(arg1)
            self.put(arg1)
        else:
            Err.throwerror("missing arguments on stack", "stack")


    def drop2(self):
        if len(self.stack) >= 2:
            self.pop()
            self.pop()
        else:
            Err.throwerror("missing arguments on stack", "stack")


    def swap2(self):
        if len(self.stack) >= 4:
            arg1 = self.pop()
            arg2 = self.pop()
            arg3 = self.pop()
            arg4 = self.pop()
            self.put(arg2)
            self.put(arg1)
            self.put(arg4)
            self.put(arg3)
        else:
            Err.throwerror("missing arguments on stack", "stack")
            

    def dropall(self):
        for key in self.stack:
            self.pop()
