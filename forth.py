#!/usr/bin/env python3


from Forth import *


class ForthBase(object):
	defquit = 'bye'

	def __init__(self):
		self.stack = Stk.Stack()
		self.commands = {
				"put": self.stack.put,
				".": self.stack.prt,
				".s": self.stack.show,
				"words": self.words,
				"+": self.stack.add,
				"-": self.stack.sup,
				"*": self.stack.mul,
				"div": self.stack.div,
				"drop": self.stack.drop,
				"swap": self.stack.swap,
				"negate": self.stack.negate,
				"dup": self.stack.dup,
				"2drop": self.stack.drop2,
				"2swap": self.stack.swap2,
				"dropall": self.stack.dropall
				# tuck pick < > ... mod over rot xor or and not
				}


	def read(self):
		return input("> ")


	def words(self):
		text = ''
		for key in self.commands:
			text = text + key + ' '

		print(text)


	def runcmd(self, array):
		if array[0] != ForthBase.defquit:
			for command in array:
				if command in self.commands:
					self.commands[command]()
				elif command.isnumeric():
					self.commands['put'](command)
				else:
					Err.throwerror("command not found", "ForthBase")

		print("OK")


	def splitstr(self, string):
		return string.split(sep=' ')


def start():
	fbase = ForthBase()

	userinput = ""

	while userinput != ForthBase.defquit:
		userinput = fbase.read()
		userarray = fbase.splitstr(userinput)
		fbase.runcmd(userarray)


if __name__ == "__main__":
	start()
