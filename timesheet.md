timesheet forth python
======================

```
|------------+-------+-------+------------------------------------------------------------|
| DATE       | START |  STOP | DESCRIPTION                                                |
|------------+------:+------:+------------------------------------------------------------|
| 2015-10-04 | 00:00 | 04:00 | making forth engine: forth input loop and output loop,     |
| til        |       |       | adding basic words such as . .s * / + -, also added        |
| 2015-10-07 |       |       | number input on the stack, implemented the stack and       |
|            |       |       | runnable code, learning how forth works and emacs          |
|------------+-------+-------+------------------------------------------------------------|
| 2015-10-08 | 17:12 | 18:02 | making extra dup word, research on extra words to add      |
|------------+-------+-------+------------------------------------------------------------|
| 2015-10-08 | 18:23 | 20:12 | making extra 2drop 2swap words and splitting forth file    |
|            |       |       | into multiple files part 1                                 |
|------------+-------+-------+------------------------------------------------------------|
| 2015-10-08 | 17:12 | 17:45 | making extra dropall word and fixing error import error    |
|------------+-------+-------+------------------------------------------------------------|
| 2015-10-08 | 20:16 | 20:33 | research to implementing "self made words" aka words       |
|            |       |       | defined at runtime and making git repo                     |
|------------+-------+-------+------------------------------------------------------------|
| 2015-10-09 | 00:21 | 00:32 | making extra functions for easy use and execution of words |
|            |       |       | probible end of project                                    |
|------------+-------+-------+------------------------------------------------------------|
```
